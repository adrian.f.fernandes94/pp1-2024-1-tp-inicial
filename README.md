# Proyecto profesional I

# TP Inicial – Aplicación de la Inteligencia Artificial

## Integrantes

* Gabriel Adrian Fariña Fernandes
* Mariano Chun
* Tomas Eduardo Esteban Mandril

## Docentes

* Juan Carlos Monteros
* Evelin Emilce Aragon

## Enunciado TP Inicial
Después de leer el material e investigar respecto a la aplicación de algoritmos de Machine Learning para Inteligencia Artificial utilizando el lenguaje Python para el procesamiento de los datos, se pide:

    1. Documentar la investigación de los temas propuestos y subirla a un repositorio de trabajo

    2. Crear un Chatbot

    3. Lograr que el login se realice con reconocimiento de la huella digital en el celular

    4. Optimizar el Chatbot


## Objetivos del TP
* Investigar la aplicación de la IA en diferentes procesos de la vida diaria, a nivel personal, profesional y
empresarial.
* Profundizar en el conocimiento de:
  -  Onbording Digital
  -  Importancia de la IA en el onboarding
  -  Proceso de Onbording Digital
      *  Análisis de un documento de identidad
      *  Biometría
      *  Listas AML (Anti Money Laundering)
  -  Normativas existentes
* Mostrar en un Chatbot dicha aplicación

## Tipos de Chatbot
- Dumb chatbots
- Chatbot con tecnología word-spotting
- Chatbot con Inteligencia Artificial
- Según el tipo de interacción

## Consignas

### Entrega 19/3
* Entregar la evidencia del repositorio en el que trabajarán
* Documentar la Investigación realizada acerca de Aplicación de la IA, Onbording Digital, Normativas
existentes
* ¿Cuál es el objetivo u objetivos que desean lograr con el Chatbot?
* Herramientas a utilizar

### Resolución
<u>Documentación de la investigación</u>

#### Onboarding digital
*¿Qué es?* 

El onboarding digital no es más que un proceso de alta de clientes tradicional pero que además verifica la identidad real de un usuario, es decir, de verificar que el usuario es quien dice ser. Generalmente se suele dar en el segmento financiero para reducir el fraude, lavado de dinero y financiamento del terrorismo, entre otras cosas. El mecanismo de alta usualmente consiste en registrar los datos biometricos de la persona, para luego validarlos con un ente regulador, esta validación puede realizarse registrando una huella digital, uso de reconocimiento facial y/o escaneo de documento nacional de identidad.

*Importancia de la IA en el onboarding*

El digital onboarding se refiere al proceso de orientar y guiar a los clientes a través de su primera interacción con un producto o servicio de manera remota, utilizando tecnologías avanzadas como la inteligencia artificial (IA). La tecnología IA juega un papel fundamental en el onboarding digital al permitir la automatización de procesos de contratación de las empresas, la mejora de la eficiencia operativa y la reducción de errores humanos. A continuación se enumeran algunos beneficios que resaltan la importancia de la IA en este proceso de onboarding:

* **Mejora la experiencia del usuario**: La IA puede identificar productos y servicios adicionales a ofrecer al usuario que podrian beneficiarlo, en base a los datos que este proporciona en el proceso de onboarding.

* **Mejora el análisis de datos**: Durante el proceso de onboarding, la IA en segundo plano puede analizar millones de datos a traves de cientos de instituciones financieras y relacionarlos de tal forma para mejorar la identificación del aplicante, obtener un rating crediticio y ayudar a la identificación de maniobras de lavado de dinero, robos de identidad o ataques de bots.

*Proceso de Onbording Digital*

* **Análisis de un documento de identidad**: Hoy en día gracias a la IA, puede realizarse de forma mas rapida e inteligente la captura de datos de un DNI. Con el reconocimiento óptico de caracteres (OCR) y la IA, por ejemplo, se pueden leer todo tipo de documentos que contengan texto y los datos pueden extraerse automáticamente, sin necesidad de un proceso manual de lectura y comprobación de documentos. Esto se realiza a partir de una serie de pasos

  1. **Ingreso del documento**: Primero, el cliente debe ingresar o sacar una foto, o subir un archivo PDF del DNI. El software recortara la imagen y optimizará la calidad de imagen.
  2. **Reconocimiento de texto**: El software de OCR analiza los patrones de luz y oscuridad que componen las letras y los números para transformar la imagen escaneada en texto. El software OCR extrae todos los datos necesarios en cuestión de segundos y en este paso puede identificar duplicados, errores o fraudes.
  3. **Procesamiento a datos estructurados**: El texto obtenido se pasa a formato JSON u otro formato, listo para ser procesado, por ejemplo, para agregarlo a una base de datos.
  4. **Extracción de firma y foto**: Para mejorar la seguridad, se puede extraer la foto y la firma del cliente, en un formato utilizable. Esto se realiza por medio de computer vision y deep learning para analizar el documento sección por sección y dividirlo en texto y componentes visuales. Luego se comprueba que componentes pueden llegar a ser una firma o foto. En caso de identificarlos, el software recorta la firma y/o foto, convirtiendolo a un formato utilizable, lo que mas adelante puede utilizarse para verificaciones de identidad por medio de selfies o comparación de firmas.


* **Biometría**: La biometría incluye las medidas biológicas, o características físicas, que se pueden utilizar para identificar a las personas, por ejemplo, la clasificación de huellas dactilares, el reconocimiento facial y los exámenes de retina son todas formas de tecnología biométrica, por mencionar algunas. En el proceso de onboarding digital, el uso de la biometría se ha convetido en uno de los mecanismos mas utilizados para validar la identidad de un usuario. Como se mencionó, uno de los métodos mas usados es el de reconocimiento facial por medio de 'selfies', gracias a que hoy en día mucha gente cuenta con smartphones, cuyas camaras de alta resolución permiten realizar esta comprobación de identidad de forma correcta y rapida.

* **Listas AML (Anti Money Laundering)**: AML es la sigla de Anti-Money Laundering o sea la Prevención del Lavado de Dinero, un sistema de prevención esencial para las actividades vinculadas tanto a las finanzas como al cumplimiento de las empresas. Son procesos estandarizados e incorporados a la legislación de los países que pueden dar la garantía de que los clientes de una empresa no están realizando lavado de dinero. Los siguiente métodos son utilizados en el proceso de onboarding digital para un chequeo efectivo de AML:

  1. **PEP (Persona expuesta politicamente)**: Enfocado en detectar clientes de alto riesgo que tienen acceso a medios ilegales para obtener dinero como podrian ser sobornos o lavado de dinero a diferencia de los clientes comunes.
  2. **RCA (Familiares y socios cercanos)**: Permite monitorear personas o empresas cuya relación es cercana a alguna PEP, lo que permite identificar cualquier riesgo de delitos financieros debido a la conexión con la PEP.
  3. **Filtro de listas negras**: Enfocado en identificar si las personas pertenecen a algun país presente en las listas negras de paises creada por el GAFI (Grupo de Acción Financiera Internacional), lista la cual forman aquellos paises que no cumplen con los estandares de anti lavado de dinero y anti financiamiento del terrorismo.
  4. **Escaneo de medios adversos**: Mecanismo por el cual se examina si el cliente cuenta con reputación adversa, al buscar antecedentes penales del mismo o si esta asociado a ellos.
  5. **Monitoreo de transacciones en la cuenta**: Enfocado en mantener el rastro de las transacciones, depositos y retiros del cliente para prevenir maniobras delictivas. 

*Normativas existentes*

------------- A COMPLETAR ------------- 

*Ejemplo de chatbot donde se aplica IA*

------------- A COMPLETAR ------------- 

#### Chatbot

*¿Que es?*
    
Un chatbot es un programa informático que utiliza inteligencia artificial (IA) y procesamiento del lenguaje natural (NLP) para comprender las preguntas de los clientes y automatizar las respuestas a dichas preguntas, simulando la conversación humana.

#### Tipos de chatbot
* **Dumb chatbot**: Tambien denominado Chatbot de ITR (Respuesta de interacción de texto), es aquel que funciona a través de una lógica secuencial, siguiendo los comandos simples que el programador introduce en él previamente. Se trata de la opción más básica y sólo es capaz de responder ante instrucciones muy específicas, es posible desarrollarlo de modo que proporcione respuestas del tipo conversacional, sacándolo de su rigidez. Puede utilizarse para responder FAQs/preguntas frecuentes o para complementar una landing page a modo de formulario.
* **Chatbot con tecnología word-spotting**: El chatbot de “Word-spotting” podría considerarse un híbrido entre los dumb chatbots y los basados en IA. Se denominan así porque funcionan “reconociendo palabras”, y en función de estas, dan una respuesta que ya está programada. Tiene un caracter mas conversacional que el dumb chatbot y a la vez quita la necesidad de un sistema basado en IA para comprender la pregunta del cliente. Por otro lado, una desventaja es que no considera la intención o el contexto al interpretar palabras clave, lo que ocasiona que sean menos precisos que los chatbots con IA.
* **Chatbot con inteligencia artificial**: Los chatbos con IA son aquellos que pueden entender las preguntas del usuario, sin importar como hayan sido redactadas. Cuando el chatbot no esta seguro de lo que la persona esta preguntando y encuentra mas de una accción que puede satisfacer el pedido, puede preguntar para esclarecer la pregunta. Inclusive, puede mostrar una lista de posibles acciones desde la cual el usuario puede seleccionar la opcion que mas se alinee con sus necesidades. Los algoritmos de machine learning que sustentan estos chatbots le permiten auto-aprender y desarrollar una base creciente de conocimiento inteligente sobre preguntas y respuestas que estan basadas en interacciones de usuario. En caso de usar deep-learning, mientras mas tiempo el chatbot este en funcionamento, de mejor forma podrá entender lo que el usuario quiere lograr y podrá proveer de respuestas mas detalladas y precisas.
* **Chatbot según el tipo de interacción**: Tenemos varios tipos de chatbots segun el tipo de interacción, que tienen como objetivo ayudar al negocio a crecer y proveer mejores servicios a los clientes, entre los que podemos encontrar:

  * **Chatbot de soporte**: Usado en servicio al cliente para completar cualquier tarea relacionada a dar soporte a clientes, como podria ser responder preguntas frecuentes, guiar a un usuario a lo largo de todo el proceso de realizar una demo o de un estadío de compra. Si el chatbot no puede satisfacer la solicitud, pasa al cliente a los agentes humanos de soporte.
  * **Chatbot de marketing y ventas**: Su proposito principal es ayudar a los clientes a explorar y comprar un producto o servicio, al enviarles informacion relevante o atractiva, intentando identificar potenciales clientes y guiandolos hasta un portal de ventas. 
  * **Chatbot de habilidades**: Son aquellos que pueden ejecutar comandos como "reproducir una cancion", "enviar un email", "hacer una llamada", etc. Para este tipo de chatbot, si se lo utiliza por medio de voz, permite que el usuario no necesita sostener el dispositivo o presionar algun boton para utilizarlo. De la misma forma, las ordenes pueden darse por medio de texto.
  * **Chatbot de entretenimiento**: Son utilizados usualmente para el único proposito de mejorar el compromiso del cliente con la marca y proveer una experiencia única y divertida a los usuarios. Suelen ser complejos de construir y suelen requerir de IA para desarrollarlos.


#### Objetivo/s a lograr con el chatbot
------------- A COMPLETAR ------------- 
#### Herramientas a utilizar
-------------  A COMPLETAR ------------- 

### Fuentes

[¿Qué es el Onboarding Digital y como influye el sector financiero?](https://www.youtube.com/watch?v=KsyEjajwhxk)

[¿Qué es el Onboarding Digital?](https://www.mobbeel.com/blog/que-es-el-onboarding-digital/)

[Artificial Intelligence Can Enhance Customer Experience in Digital Onboarding](https://www.doxim.com/ai-can-enhance-customer-experience-in-digital-onboarding/)

[Extracción de datos y verificación de identidad con DNI y pasaportes](https://www.klippa.com/es/blog/informativo/verificacion-de-identidad/)
[Qué es un chatbot?](https://www.ibm.com/es-es/topics/chatbots)

[¿Qué es la biometría y cómo se utiliza en la seguridad?](https://latam.kaspersky.com/resource-center/definitions/biometrics)
[Tipos de chatbot: ventajas y características](https://centribal.com/es/tipos-de-chatbot-ventajas-y-caracteristicas/)

[La validación de identidad y su impacto en el onboarding digital](https://reconoserid.com/la-validacion-de-identidad-y-su-impacto-en-el-onboarding-digital/)

[¿Qué es AML (Anti Money Laundering) y cómo funciona el monitoreo de lavado de dinero?](https://blog.truora.com/es/aml)

[Digital Onboarding and Anti Money Laundering (AML)](https://fineksus.com/digital-onboarding-and-anti-money-laundering-aml/)

[Revoluciona tus conversaciones. Descubre los distintos tipos de Chatbots](https://blog.convertia.com/index.php/revoluciona-tus-conversaciones-descubre-los-distintos-tipos-de-chatbots/)

[10 Types of Chatbots: Key Specifics, Mechanics, and Areas of Usage](https://helpcrunch.com/blog/types-of-chatbots/#Types_of_chatbots_based_on_where_they_are_used)